$('.slid-gallery').slick({
    autoplay:true,
    speed:2000,
    autoplaySpeed:2000,
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows:false,
    dots:true,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});
$('.slid-partner').slick({
    arrows:true,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay:false,
    autoplaySpeed:2000,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});